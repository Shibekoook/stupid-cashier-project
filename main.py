def get_total_HT(total_HT = 0):
	while True:
		try:
			total_HT = float(input('please enter your HT\n'))
		except ValueError:
			print('please try again the HT must be a number :))')
			continue
		else:
			return total_HT
			break

def get_TVA(TVA = 0):
	while True:
		try:
			TVA = float(input('please enter your TVA\n'))
		except ValueError:
			print('please try again the TVA must be a number :))')
			continue
		else:
			return TVA
			break

def calculate_TTC():
	total_HT = get_total_HT()
	TVA = get_TVA() 
	TTC = total_HT + (total_HT * TVA)
	return TTC

print(f'your TTC is {calculate_TTC()}')